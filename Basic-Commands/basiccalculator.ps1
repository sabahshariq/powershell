﻿#This example demonstrate how to use if-else statement in powershell

cls
Write-Host "Basic Calculator"

for($i=0; $i-le 15; $i++)
{
    Write-Host -NoNewline "="
}

"`n"
$a=Read-Host "Enter value of a"
$b=Read-Host "Enter value of b"

"`n"
Write-Host "1. Addition"
Write-Host "2. Subtraction"
Write-Host "3. Multiplication"
Write-Host "4. Division"

"`n"
$o=Read-Host "which operation do you want to perform?"

if($o -eq 1)
{
    $result=($a+$b)
    Write-Host "`nResult is:" `n$result
}
elseif($o -eq 2)
{
    $result=($a-$b)
    Write-Host "`nResult is:" $result
}
elseif($o -eq 3)
{
    $result=($a*$b)
    Write-Host "`nResult is:" $result
}
else
{
    $result=($a/$b)
    Write-Host "`nResult is:" $result
}

